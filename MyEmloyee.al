table 50100 MyEmployee
{
    CaptionML = ENU='Employees',PLK='Pracownicy';
        
    fields
    {
        field(50100;Id; Code[20])
        {
            CaptionML = ENU='Id',PLK='Identyfikator';
        }

        field(50101; FirstName; Text[50])
        {
            CaptionML = ENU='First Name',PLK='Imię';
        }

        field(50102; LastName; Text[50])
        {
            CaptionML = ENU='Last Name',PLK='Nazwisko';
        }
    }
    
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    
    var
    
    trigger OnInsert()
    begin
        
    end;
    
    trigger OnModify()
    begin
        
    end;
    
    trigger OnDelete()
    begin
        
    end;
    
    trigger OnRename()
    begin
        
    end;
    
}