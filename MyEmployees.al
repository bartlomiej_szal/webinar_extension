page 50100 MyEmployees
{
    PageType = List;
    SourceTable = MyEmployee;
    CaptionML = ENU='Employees',PLK='Pracownicy';
    
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Id; Id)
                {
                    ApplicationArea = All;
                }

                field(FirstName; FirstName)
                {
                    ApplicationArea = All;
                }

                field(LastName; LastName)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    
    actions
    {
        
    }
    
    var
}
